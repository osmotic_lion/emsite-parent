/**
 * Copyright &copy; 2017 <a href="https://gitee.com/hackempire/emsite-parent">emsite</a> All rights reserved.
 */
package com.empire.emsite.modules.cms.facade;

import java.util.List;

import com.empire.emsite.common.persistence.Page;
import com.empire.emsite.modules.cms.entity.Article;

/**
 * 类ArticleFacadeService.java的实现描述：文章FacadeService接口
 * 
 * @author arron 2017年9月17日 下午9:42:29
 */
public interface ArticleFacadeService {
    public Article get(String id);

    public Page<Article> findPage(Page<Article> page, Article article, boolean isDataScopeFilter);

    public void save(Article article);

    public void delete(Article article, Boolean isRe);

    /**
     * 通过编号获取内容标题
     * 
     * @return new Object[]{栏目Id,文章Id,文章标题}
     */
    public List<Object[]> findByIds(String ids);

    /**
     * 点击数加一
     */
    public void updateHitsAddOne(String id);

    /**
     * 更新索引
     */
    public void createIndex();

    /**
     * 全文检索
     */
    //FIXME 暂不提供检索功能
    public Page<Article> search(Page<Article> page, String q, String categoryId, String beginDate, String endDate);

}
