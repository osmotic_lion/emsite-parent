/**
 * Copyright &copy; 2017 <a href="https://gitee.com/hackempire/emsite-parent">emsite</a> All rights reserved.
 */
package com.empire.emsite.modules.testwx.facade;

import java.util.List;

import com.empire.emsite.common.persistence.Page;
import com.empire.emsite.modules.testwx.entity.TestWxData;

/**
 * 类TestWxDataFacadeService.java的实现描述：TestwxFacadeService
 * 
 * @author arron 2017年10月30日 下午1:04:06
 */
public interface TestWxDataFacadeService {

    public TestWxData get(String id);

    public List<TestWxData> findList(TestWxData testWxData);

    public Page<TestWxData> findPage(Page<TestWxData> page, TestWxData testWxData);

    public void save(TestWxData testWxData);

    public void delete(TestWxData testWxData);

}
