/**
 * Copyright &copy; 2017 <a href="https://gitee.com/hackempire/emsite-parent">emsite</a> All rights reserved.
 */
package com.empire.emsite.test.facade;

import java.util.List;

import com.empire.emsite.common.persistence.Page;
import com.empire.emsite.test.entity.TestDataMain;

/**
 * 类TestDataMainFacadeService.java的实现描述：主子表生成FacadeService接口
 * 
 * @author arron 2017年9月17日 下午9:57:19
 */
public interface TestDataMainFacadeService {

    public TestDataMain get(String id);

    public List<TestDataMain> findList(TestDataMain testDataMain);

    public Page<TestDataMain> findPage(Page<TestDataMain> page, TestDataMain testDataMain);

    public void save(TestDataMain testDataMain);

    public void delete(TestDataMain testDataMain);

}
