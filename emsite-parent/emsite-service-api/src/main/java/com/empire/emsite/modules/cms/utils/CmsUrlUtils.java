/**
 * Copyright &copy; 2017 <a href="https://gitee.com/hackempire/emsite-parent">emsite</a> All rights reserved.
 */
package com.empire.emsite.modules.cms.utils;

import javax.servlet.ServletContext;

import com.empire.emsite.common.utils.SpringContextHolder;

/**
 * 类CmsUrlUtils.java的实现描述：栏目动态URL地址和图片地址获取方法声明，
 * 
 * @author arron 2017年10月12日 上午12:09:34
 */
public class CmsUrlUtils {
    private static ServletContext context = SpringContextHolder.getBean(ServletContext.class);

    /**
     * 获得文章动态URL地址
     * 
     * @param article
     * @return url
     */
    //    public static String getUrlDynamic(Article article) {
    //        if (StringUtils.isNotBlank(article.getLink())) {
    //            return article.getLink();
    //        }
    //        StringBuilder str = new StringBuilder();
    //        str.append(context.getContextPath()).append(Global.getFrontPath());
    //        str.append("/view-").append(article.getCategory().getId()).append("-").append(article.getId())
    //                .append(Global.getUrlSuffix());
    //        return str.toString();
    //    }

    /**
     * 获得栏目动态URL地址
     * 
     * @param category
     * @return url
     */
    //    public static String getUrlDynamic(Category category) {
    //        if (StringUtils.isNotBlank(category.getHref())) {
    //            if (!category.getHref().contains("://")) {
    //                return context.getContextPath() + Global.getFrontPath() + category.getHref();
    //            } else {
    //                return category.getHref();
    //            }
    //        }
    //        StringBuilder str = new StringBuilder();
    //        str.append(context.getContextPath()).append(Global.getFrontPath());
    //        str.append("/list-").append(category.getId()).append(Global.getUrlSuffix());
    //        return str.toString();
    //    }

    /**
     * 从图片地址中加入ContextPath地址
     * 
     * @param src
     * @return src
     */
    //    public static String formatImageSrcToWeb(String src) {
    //        if (StringUtils.isBlank(src))
    //            return src;
    //        if (src.startsWith(context.getContextPath() + "/userfiles")) {
    //            return src;
    //        } else {
    //            return context.getContextPath() + src;
    //        }
    //    }
}
