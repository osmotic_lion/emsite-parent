/**
 * Copyright &copy; 2017 <a href="https://gitee.com/hackempire/emsite-parent">emsite</a> All rights reserved.
 */
package com.empire.emsite.modules.cms.facade;

import java.util.List;

import com.empire.emsite.common.persistence.Page;
import com.empire.emsite.modules.cms.entity.Link;

/**
 * 类LinkFacadeService.java的实现描述：链接FacadeService接口
 * 
 * @author arron 2017年9月17日 下午9:44:04
 */
public interface LinkFacadeService {
    public Link get(String id);

    public Page<Link> findPage(Page<Link> page, Link link, boolean isDataScopeFilter);

    public void delete(Link link, Boolean isRe);

    public void save(Link link);

    /**
     * 通过编号获取内容标题
     */
    public List<Object[]> findByIds(String ids);

}
