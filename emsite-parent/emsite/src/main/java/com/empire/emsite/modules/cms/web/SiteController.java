/**
 * Copyright &copy; 2017 <a href="https://gitee.com/hackempire/emsite-parent">emsite</a> All rights reserved.
 */
package com.empire.emsite.modules.cms.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.empire.emsite.common.config.MainConfManager;
import com.empire.emsite.common.persistence.Page;
import com.empire.emsite.common.utils.CookieUtils;
import com.empire.emsite.common.utils.StringUtils;
import com.empire.emsite.common.web.BaseController;
import com.empire.emsite.modules.cms.entity.Site;
import com.empire.emsite.modules.cms.facade.SiteFacadeService;
import com.empire.emsite.modules.cms.utils.CmsUtils;
import com.empire.emsite.modules.sys.utils.UserUtils;

/**
 * 类SiteController.java的实现描述：站点Controller
 * 
 * @author arron 2017年10月30日 下午7:12:46
 */
@Controller
@RequestMapping(value = "${adminPath}/cms/site")
public class SiteController extends BaseController {

    @Autowired
    private SiteFacadeService siteFacadeService;

    @ModelAttribute
    public Site get(@RequestParam(required = false) String id) {
        if (StringUtils.isNotBlank(id)) {
            return siteFacadeService.get(id);
        } else {
            return new Site();
        }
    }

    @RequiresPermissions("cms:site:view")
    @RequestMapping(value = { "list", "" })
    public String list(Site site, HttpServletRequest request, HttpServletResponse response, Model model) {
        site.setCurrentUser(UserUtils.getUser());
        Page<Site> page = siteFacadeService.findPage(new Page<Site>(request, response), site);
        model.addAttribute("page", page);
        return "modules/cms/siteList";
    }

    @RequiresPermissions("cms:site:view")
    @RequestMapping(value = "form")
    public String form(Site site, Model model) {
        model.addAttribute("site", site);
        return "modules/cms/siteForm";
    }

    @RequiresPermissions("cms:site:edit")
    @RequestMapping(value = "save")
    public String save(Site site, Model model, RedirectAttributes redirectAttributes) {
        if (MainConfManager.isDemoMode()) {
            addMessage(redirectAttributes, "演示模式，不允许操作！");
            return "redirect:" + adminPath + "/cms/site/?repage";
        }
        if (!beanValidator(model, site)) {
            return form(site, model);
        }
        site.setCurrentUser(UserUtils.getUser());
        siteFacadeService.save(site);
        CmsUtils.removeCache("siteList");
        addMessage(redirectAttributes, "保存站点'" + site.getName() + "'成功");
        return "redirect:" + adminPath + "/cms/site/?repage";
    }

    @RequiresPermissions("cms:site:edit")
    @RequestMapping(value = "delete")
    public String delete(Site site, @RequestParam(required = false) Boolean isRe, RedirectAttributes redirectAttributes) {
        if (MainConfManager.isDemoMode()) {
            addMessage(redirectAttributes, "演示模式，不允许操作！");
            return "redirect:" + adminPath + "/cms/site/?repage";
        }
        if (Site.isDefault(site.getId())) {
            addMessage(redirectAttributes, "删除站点失败, 不允许删除默认站点");
        } else {
            siteFacadeService.delete(site, isRe);
            //CmsUtils.removeCache("site_" + site.getId());
            CmsUtils.removeCache("siteList");
            addMessage(redirectAttributes, (isRe != null && isRe ? "恢复" : "") + "删除站点成功");
        }
        return "redirect:" + adminPath + "/cms/site/?repage";
    }

    /**
     * 选择站点
     * 
     * @param id
     * @return
     */
    @RequiresPermissions("cms:site:select")
    @RequestMapping(value = "select")
    public String select(String id, boolean flag, HttpServletResponse response) {
        if (id != null) {
            UserUtils.putCache("siteId", id);
            // 保存到Cookie中，下次登录后自动切换到该站点
            CookieUtils.setCookie(response, "siteId", id);
        }
        if (flag) {
            return "redirect:" + adminPath;
        }
        return "modules/cms/siteSelect";
    }
}
