/**
 * Copyright &copy; 2017 <a href="https://gitee.com/hackempire/emsite-parent">emsite</a> All rights reserved.
 */
package com.empire.emsite.modules.cms.web;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.empire.emsite.common.web.BaseController;
import com.empire.emsite.modules.cms.entity.Site;
import com.empire.emsite.modules.cms.facade.SiteFacadeService;
import com.empire.emsite.modules.cms.service.FileTplService;
import com.empire.emsite.modules.cms.utils.CmsUtils;

/**
 * 类TemplateController.java的实现描述：站点Controller
 * 
 * @author arron 2017年10月30日 下午7:13:30
 */
@Controller
@RequestMapping(value = "${adminPath}/cms/template")
public class TemplateController extends BaseController {

    @Autowired
    private FileTplService    fileTplService;
    @Autowired
    private SiteFacadeService siteFacadeService;

    @RequiresPermissions("cms:template:edit")
    @RequestMapping(value = "")
    public String index() {
        return "modules/cms/tplIndex";
    }

    @RequiresPermissions("cms:template:edit")
    @RequestMapping(value = "tree")
    public String tree(Model model) {
        Site site = siteFacadeService.get(CmsUtils.getCurrentSiteId());
        model.addAttribute("templateList", fileTplService.getListForEdit(site.getSolutionPath()));
        return "modules/cms/tplTree";
    }

    @RequiresPermissions("cms:template:edit")
    @RequestMapping(value = "form")
    public String form(String name, Model model) {
        model.addAttribute("template", fileTplService.getFileTpl(name));
        return "modules/cms/tplForm";
    }

    @RequiresPermissions("cms:template:edit")
    @RequestMapping(value = "help")
    public String help() {
        return "modules/cms/tplHelp";
    }
}
