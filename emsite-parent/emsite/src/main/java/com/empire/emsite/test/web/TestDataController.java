/**
 * Copyright &copy; 2017 <a href="https://gitee.com/hackempire/emsite-parent">emsite</a> All rights reserved.
 */
package com.empire.emsite.test.web;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.empire.emsite.common.config.MainConfManager;
import com.empire.emsite.common.persistence.Page;
import com.empire.emsite.common.utils.StringUtils;
import com.empire.emsite.common.web.BaseController;
import com.empire.emsite.modules.sys.utils.UserUtils;
import com.empire.emsite.test.entity.TestData;
import com.empire.emsite.test.facade.TestDataFacadeService;
import com.unj.dubbotest.provider.DemoService;

/**
 * 类TestDataController.java的实现描述：单表生成Controller
 * 
 * @author arron 2017年10月30日 下午7:25:38
 */
@Controller
@RequestMapping(value = "${adminPath}/test/testData")
public class TestDataController extends BaseController {

    /*
     * @Autowired private TestDataService testDataService;
     */
    @Autowired
    private DemoService           demoService;

    @Autowired
    private TestDataFacadeService testDataFacadeService;

    @ModelAttribute
    public TestData get(@RequestParam(required = false) String id) {
        TestData entity = null;
        if (StringUtils.isNotBlank(id)) {
            entity = testDataFacadeService.get(id);
        }
        if (entity == null) {
            entity = new TestData();
        }
        return entity;
    }

    @RequiresPermissions("test:testData:view")
    @RequestMapping(value = { "list", "" })
    public String list(TestData testData, HttpServletRequest request, HttpServletResponse response, Model model) {
        Page<TestData> page = testDataFacadeService.findPage(new Page<TestData>(request, response), testData);
        model.addAttribute("page", page);
        String hello = demoService.sayHello("guoxue");
        System.out.println(hello);
        List<TestData> x = testDataFacadeService.findList(testData);
        System.out.println("dubbo查询list" + x.size());
        List list = demoService.getUsers();
        if (list != null && list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                System.out.println(list.get(i));
            }
        }
        return "emsite/test/testDataList";
    }

    @RequiresPermissions("test:testData:view")
    @RequestMapping(value = "form")
    public String form(TestData testData, Model model) {
        model.addAttribute("testData", testData);
        return "emsite/test/testDataForm";
    }

    @RequiresPermissions("test:testData:edit")
    @RequestMapping(value = "save")
    public String save(TestData testData, Model model, RedirectAttributes redirectAttributes) {
        if (!beanValidator(model, testData)) {
            return form(testData, model);
        }
        testData.setCurrentUser(UserUtils.getUser());
        testDataFacadeService.save(testData);
        addMessage(redirectAttributes, "保存单表成功");
        return "redirect:" + MainConfManager.getAdminPath() + "/test/testData/?repage";
    }

    @RequiresPermissions("test:testData:edit")
    @RequestMapping(value = "delete")
    public String delete(TestData testData, RedirectAttributes redirectAttributes) {
        testDataFacadeService.delete(testData);
        addMessage(redirectAttributes, "删除单表成功");
        return "redirect:" + MainConfManager.getAdminPath() + "/test/testData/?repage";
    }

}
