/**
 * Copyright &copy; 2017 <a href="https://gitee.com/hackempire/emsite-parent">emsite</a> All rights reserved.
 */
package com.empire.emsite.modules.cms.web;

import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.empire.emsite.common.web.BaseController;
import com.empire.emsite.modules.cms.entity.Category;
import com.empire.emsite.modules.cms.facade.StatsFacadeService;
import com.empire.emsite.modules.cms.utils.CmsUtils;

/**
 * 类StatsController.java的实现描述：统计Controller
 * 
 * @author arron 2017年10月30日 下午7:13:08
 */
@Controller
@RequestMapping(value = "${adminPath}/cms/stats")
public class StatsController extends BaseController {

    @Autowired
    private StatsFacadeService statsFacadeService;

    /**
     * 文章信息量
     * 
     * @param paramMap
     * @param model
     * @return
     */
    @RequiresPermissions("cms:stats:article")
    @RequestMapping(value = "article")
    public String article(@RequestParam Map<String, Object> paramMap, Model model) {
        List<Category> list = statsFacadeService.article(paramMap, CmsUtils.getCurrentSiteId());
        model.addAttribute("list", list);
        model.addAttribute("paramMap", paramMap);
        return "modules/cms/statsArticle";
    }

}
