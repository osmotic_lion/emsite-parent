/**
 * Copyright &copy; 2017 <a href="https://gitee.com/hackempire/emsite-parent">emsite</a> All rights reserved.
 */
package com.empire.emsite.modules.cms.web;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.empire.emsite.common.web.BaseController;
import com.empire.emsite.modules.cms.facade.CategoryFacadeService;
import com.empire.emsite.modules.cms.utils.CmsUtils;
import com.empire.emsite.modules.sys.utils.UserUtils;

/**
 * 类CmsController.java的实现描述：内容管理Controller
 * 
 * @author arron 2017年10月30日 下午7:10:58
 */
@Controller
@RequestMapping(value = "${adminPath}/cms")
public class CmsController extends BaseController {

    @Autowired
    private CategoryFacadeService categoryFacadeService;

    @RequiresPermissions("cms:view")
    @RequestMapping(value = "")
    public String index() {
        return "modules/cms/cmsIndex";
    }

    @RequiresPermissions("cms:view")
    @RequestMapping(value = "tree")
    public String tree(Model model) {
        model.addAttribute("categoryList",
                categoryFacadeService.findByUser(true, null, UserUtils.getUser(), CmsUtils.getCurrentSiteId()));
        return "modules/cms/cmsTree";
    }

    @RequiresPermissions("cms:view")
    @RequestMapping(value = "none")
    public String none() {
        return "modules/cms/cmsNone";
    }

}
