/**
 * Copyright &copy; 2017 <a href="https://gitee.com/hackempire/emsite-parent">emsite</a> All rights reserved.
 */
package com.empire.emsite.modules.testwx.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.empire.emsite.common.config.MainConfManager;
import com.empire.emsite.common.persistence.Page;
import com.empire.emsite.common.utils.StringUtils;
import com.empire.emsite.common.web.BaseController;
import com.empire.emsite.modules.sys.utils.UserUtils;
import com.empire.emsite.modules.testwx.entity.TestWxData;
import com.empire.emsite.modules.testwx.facade.TestWxDataFacadeService;

/**
 * 类TestWxDataController.java的实现描述：bbController
 * 
 * @author arron 2017年10月30日 下午7:25:09
 */
@Controller
@RequestMapping(value = "${adminPath}/testwx/testWxData")
public class TestWxDataController extends BaseController {

    @Autowired
    private TestWxDataFacadeService testWxDataFacadeService;

    @ModelAttribute
    public TestWxData get(@RequestParam(required = false) String id) {
        TestWxData entity = null;
        if (StringUtils.isNotBlank(id)) {
            entity = testWxDataFacadeService.get(id);
        }
        if (entity == null) {
            entity = new TestWxData();
        }
        return entity;
    }

    @RequiresPermissions("testwx:testWxData:view")
    @RequestMapping(value = { "list", "" })
    public String list(TestWxData testWxData, HttpServletRequest request, HttpServletResponse response, Model model) {
        testWxData.setCurrentUser(UserUtils.getUser());
        Page<TestWxData> page = testWxDataFacadeService.findPage(new Page<TestWxData>(request, response), testWxData);
        model.addAttribute("page", page);
        return "modules/testwx/testWxDataList";
    }

    @RequiresPermissions("testwx:testWxData:view")
    @RequestMapping(value = "form")
    public String form(TestWxData testWxData, Model model) {
        model.addAttribute("testWxData", testWxData);
        return "modules/testwx/testWxDataForm";
    }

    @RequiresPermissions("testwx:testWxData:edit")
    @RequestMapping(value = "save")
    public String save(TestWxData testWxData, Model model, RedirectAttributes redirectAttributes) {
        if (!beanValidator(model, testWxData)) {
            return form(testWxData, model);
        }
        testWxData.setCurrentUser(UserUtils.getUser());
        testWxDataFacadeService.save(testWxData);
        addMessage(redirectAttributes, "保存wx成功");
        return "redirect:" + MainConfManager.getAdminPath() + "/testwx/testWxData/?repage";
    }

    @RequiresPermissions("testwx:testWxData:edit")
    @RequestMapping(value = "delete")
    public String delete(TestWxData testWxData, RedirectAttributes redirectAttributes) {
        testWxDataFacadeService.delete(testWxData);
        addMessage(redirectAttributes, "删除wx成功");
        return "redirect:" + MainConfManager.getAdminPath() + "/testwx/testWxData/?repage";
    }

}
