/**
 * Copyright &copy; 2017 <a href="https://gitee.com/hackempire/emsite-parent">emsite</a> All rights reserved.
 */
package com.empire.emsite.modules.testwx.facade.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.empire.emsite.common.persistence.Page;
import com.empire.emsite.modules.testwx.entity.TestWxData;
import com.empire.emsite.modules.testwx.facade.TestWxDataFacadeService;
import com.empire.emsite.modules.testwx.service.TestWxDataService;

/**
 * 类TestWxDataFacadeServiceImpl.java的实现描述：bbFacadeServiceImpl
 * 
 * @author arron 2017年10月30日 下午4:37:25
 */
@Service
public class TestWxDataFacadeServiceImpl implements TestWxDataFacadeService {
    @Autowired
    private TestWxDataService testWxDataService;

    public TestWxData get(String id) {
        return testWxDataService.get(id);
    }

    public List<TestWxData> findList(TestWxData testWxData) {
        return testWxDataService.findList(testWxData);
    }

    public Page<TestWxData> findPage(Page<TestWxData> page, TestWxData testWxData) {
        return testWxDataService.findPage(page, testWxData);
    }

    public void save(TestWxData testWxData) {
        testWxDataService.save(testWxData);
    }

    public void delete(TestWxData testWxData) {
        testWxDataService.delete(testWxData);
    }

}
